//****************************************************************************************
//
//	INCLUDE FILES
//
//****************************************************************************************
#include	<fstream>
#include	<iostream>
#include	<string>
#include <algorithm>

// #include	<unistd.h>

#include	"DocumentIndex.h"
#include	"GetLine.h"

using namespace std;


typedef	string::size_type	StringSize;



//****************************************************************************************
//
//	DocumentFile::Close
//
//****************************************************************************************
void	DocumentFile::Close()
{
	//************************************************************************************
	//	LOCAL DATA

	//************************************************************************************
	//	EXECUTABLE STATEMENTS
	file_.close();
	file_.clear();

	return;
}

//****************************************************************************************
//
//	DocumentFile::GetPageNumber
//
//****************************************************************************************
int	DocumentFile::GetPageNumber()
{

	return(pageNumber_);
}

//****************************************************************************************
//
//	DocumentFile::GetWord
//
//****************************************************************************************

string	DocumentFile::GetWord()
{
	//************************************************************************************
	//	LOCAL DATA
	char valid[] = " .,;:?!"; //Valid spacing characters
	char invalid[] = "'\"()"; //Invalid characters that must be stripped

	for (int i = 0; i < 5; i++){ //Loop through all the invalid spacing chars
		for (int x = 0; x < text_.size(); x++){ //Loop through each char in the text till next spacing is found
			if (text_[x] == invalid[i]){ //If the char matches an invalid char
				if (invalid[i] == '\''){ //If the char is an apostraphe 
					if (tolower(text_[x + 1]) == 's'){ //If the next char is a s remove both
						int p = text_.find("'s");
						if (p != std::string::npos){
							text_.erase(p, text_.length()); //remove the 's
						}
					}
					else{
						text_.erase(remove(text_.begin(), text_.end(), invalid[i]), text_.end()); //Just remove the '
					}
				}
				else{
					text_.erase(remove(text_.begin(), text_.end(), invalid[i]), text_.end()); //Remove the illegal char
				}
			}
		}
	}

	string	word; //Word variable
	int closestSplitter = -1, tempSplit = 0;
	for (int i = 0; i < 7; i++){ //Loop through the splitter chars
		while (text_.find_first_of(valid[i]) == 0){ //Get rid of double spaces or double chars
			text_ = text_.substr(1);
		}
		tempSplit = text_.find_first_of(valid[i]); //Find the next splitter value
		if (closestSplitter == -1 || ((tempSplit > -1) && (tempSplit < closestSplitter)))
			closestSplitter = tempSplit; //Assign the lowest splitter
	}
	if (closestSplitter == -1){ //Meaning there is no more splitters
		word = text_; //Whatever is left is the word
		text_ = ""; //Since its the end signify it by empty string
	}
	else{
		word = text_.substr(0, closestSplitter); //Set the word to be from start till next splitter
		text_ = text_.substr(closestSplitter + 1); //Add the one to get rid of splitter char
		if (text_.empty() || text_ == " " || text_.size() == 0 || ((text_[0] == ' ') && (text_.size() == 1))){
			return ""; //Return an empty string if the text is empty
		}
	}

	//Loop through each character in the word
	for (int i = 0; i < word.size(); i++){
		if (isdigit(word[i])){ //If it is a digit 0-9
			word = GetWord(); //Recursively get next valid word
			break;
		}
		if (!isalpha(word[i])){ //if it isn't a alpha char
			word = GetWord(); //Get the next valid word
			break;
		}

	}

	//Loop thjrough all the excluded words
	for (int i = 0; i < exclusions.size(); i++){
		if (exclusions.at(i) == word) //If it is an excluded word
			word = GetWord(); //Recursively get a new word.
	}

	return(word);
}

//****************************************************************************************
//
//	DocumentFile::LoadExclusions
//
//****************************************************************************************
bool	DocumentFile::LoadExclusions(const string& name)
{
	//************************************************************************************
	//	LOCAL DATA
	bool	success = false;
	ifstream input(name); //Load in the excluded file list
	success = !input.fail(); //Return whether or not it worked
	string word;
	while (getline(input, word)){ //Get all the excluded words
		exclusions.push_back(word); //Add it to the excluded vector
	}

	return(success);
}

//****************************************************************************************
//
//	DocumentFile::Open
//
//****************************************************************************************
bool	DocumentFile::Open(const string& name)
{
	//************************************************************************************
	//	LOCAL DATA

	//************************************************************************************
	//	EXECUTABLE STATEMENTS
	file_.open(name, ios::in);
	if (!file_.fail())
	{
		//	You may add any useful initialization here.

		return(true);
	}
	else
	{
		return(false);
	}
}

//****************************************************************************************
//
//	DocumentFile::Read
//
//****************************************************************************************
bool	DocumentFile::Read()
{
	//************************************************************************************
	//	LOCAL DATA
	bool	success;
	success = GetLine(file_, text_); //Load in the next line

	if (file_.fail())
		success = false;


	if (text_.empty()){ //If it is a new page entry
		success = GetLine(file_, text_); // Get next line
		if (text_.empty()){ //If two lines are empty
			success = GetLine(file_, text_); //Return the next line
			pageNumber_++; //Increment page number
		}
	}


	return(success);
}

//****************************************************************************************
//
//	DocumentIndex::Create
//
//****************************************************************************************
void	DocumentIndex::Create(DocumentFile& documentFile)
{
	bool success = false;
	string word;
	while (true)
	{
		success = documentFile.Read(); //Read in the next line value
		if (!success) //If it fails to read break the loop meaning no more values to read
		{
			break;
		}
		while (true) //Keep going till we manually break the loop
		{
			word = documentFile.GetWord(); //Get the next valid word
			if (!word.empty()) //Make sure isn't empty string
			{
				Word *indexWord = NULL; //Temp word
				map<string, Word>::iterator it = index.begin(); //Starting location for the map
				while (it != index.end()){ //Loop through each value in the map
					if (it->first == word){ //If the word in the map matched current word
						indexWord = &(it->second); //Get the current Word object
						indexWord->addPage(documentFile.GetPageNumber()); //Add the current page as an occurance
						indexWord->increaseAmount(); //Increase occurance by one
					}
					it++; //Go to the next value in the list
				}
				if (indexWord == NULL){ //If the word doesn't have a Word object in the map
					//Add a new entry with the word as the key, and a Word object as the value
					index.insert(pair<string, Word>(word, Word(documentFile.GetPageNumber()))); //Creates a new Word object with page number
				}
			}
			else
			{
				break; //Break it if the word is empty.
			}
		}
	}
	

	return;
}

//****************************************************************************************
//
//	DocumentIndex::Write
//
//****************************************************************************************
void	DocumentIndex::Write(ostream& indexStream)
{
	map<string, Word>::iterator it = index.begin(); //Start of the map
	while (it != index.end()){ //Loop through all values in the map
		if (it->second.getAmount() > 10){ //If the word appeared more than 10 times
			it++; //Move onto the next word
			continue;
		}
		indexStream << it->first << " "; //Write the word out in the file
		int count = 0;
		for (auto page : it->second.getPages()){ //Loop through all the pages in the set
			if (it->second.getPages().size() > 1){ //If it is greater than one 
				count++; //Increase count
				if (count == it->second.getPages().size()){ //Account for commas
					indexStream << page << endl; //Write out page number
				}
				else{
					indexStream << page << ", "; //Write out page number with comma
				}
			}
			else{
				indexStream << page << endl; //Write out page number
			}
		}
		it++;
	}
	return;
}
