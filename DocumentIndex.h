#ifndef	DocumentIndex_h
#define	DocumentIndex_h

//****************************************************************************************
//
//	INCLUDE FILES
//
//****************************************************************************************
#include	<fstream>
#include	<map>
#include	<set>
#include	<string>
#include <vector>

using namespace std;

//****************************************************************************************
//
//	CONSTANT DEFINITIONS
//
//****************************************************************************************

//****************************************************************************************
//
//	CLASSES, TYPEDEFS AND STRUCTURES
//
//****************************************************************************************
typedef	string::size_type	StringSize;

class Word{ //Declare a word object
public:
	Word(int page){ //Constructor, given initial page
		pages.insert(page); //Add the page to the set of pages
		amount = 1; //Since its being created its first entry of word
	}
	int getAmount(){ //Get how many times it has appeared
		return amount;
	}
	set<int> getPages(){ //Get a set of all the pages
		return pages;
	}
	/*
	* Adds the specified page to the set. Since it is a set, if the
	* page already exists it won't do anything, this way you don't have
	* multiple entries from same page
	*/
	void addPage(int page){
		pages.insert(page);
	}
	//Everytime the word occurs increase the amount
	void increaseAmount(){
		amount++;
	}
private:
	int amount; //How many times it has appeared
	set<int> pages; //A set of all the pages
};

class	DocumentFile
{
	public:
		DocumentFile() : pageNumber_(1) { }
		
		void	Close();
		
		int		GetPageNumber();			//	Returns the current pge number.
		
		string	GetWord();					//	Returns the next legal word not on the exception list;
											//	returns an empty string if there are no more words in
											//	the line.
		
		bool	LoadExclusions(const string& name);	//	Loads a list of words to be excluded from the index
													//	from a file of the given name.
		
		bool	Open(const string& name);	//	Opens a document file of the given name.
		
		bool	Read();						//	Reads the next line of the document file, skipping over
											//	the double empty lines that mark page separations.
											//	Returns false if there are no more lines in the file.
		
	private:
		StringSize	beginPosition_;
		
		fstream		file_;
		
		int			pageNumber_;
		
		string		text_;

		vector<string> exclusions; //A vector containing all the excluded words

		
};

class	DocumentIndex
{
	public:
		void	Create(DocumentFile& documentFile);	//	Creates an index for the given document file.

		void	Show(ostream& stream);

		void	Write(ostream& indexFile);	//	Writes the index to the given file.
											//	The argument is a stream so that this function
											//	can be called to wrtite its output to cout for
											//	test purposes.
	
	private:
		//The index which is a map with the word as a string, then a corresponding word object
		map<string, Word> index; 
};

#endif
